import QtQuick 2.9

import org.kde.plasma.core 2.0 as Plasmacore

Item {
    Timer {
        id: stateTimer
        interval: 1000; running: true; repeat: true
        onTriggered: {
            if(helloText.state == "upsideDown")
                helloText.state = ""
            else
                helloText.state = "upsideDown"
        }
    }

    Rectangle {
        id: page

        color: "darkgray"
        anchors.fill: parent

        Text {
            id: helloText
            text: "Hello World!"
            fontSizeMode: Text.VerticalFit
            y: 50
            anchors.horizontalCenter: page.horizontalCenter
            font.pointSize: 24; font.bold: true

            MouseArea {
                id: mouseArea
                anchors.fill: parent
            }

            states: State {
                name: "upsideDown"
                when: stateTimer.onTriggered

                PropertyChanges {
                    target: helloText
                    y: parent.height - 74
                    rotation: 180
                    color: "red"
                }
            }

            transitions: Transition {
                from: ""
                to: "upsideDown"
                reversible: true

                ParallelAnimation {
                    NumberAnimation {
                        properties: "y, rotation"
                        duration: 500
                        easing.type: Easing.InOutQuad
                    }

                    ColorAnimation {
                        duration: 500
                    }
                }
            }
        }
    }
}

/*##^## Designer {
    D{i:2;anchors_height:480}
}
 ##^##*/
