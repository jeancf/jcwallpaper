import QtQuick 2.9
import QtQuick.Layouts 1.3

import org.kde.plasma.core 2.0 as PlasmaCore

ColumnLayout {
    id: root
    anchors.fill: parent

    property string cfg_configurationString

    RowLayout {
        id: someText
        anchors.top: parent.top

        Text {
            id: configText
            text: cfg_configurationString
        }
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
