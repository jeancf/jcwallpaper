# Template for a plasma wallpaper plasmoid

## Installation

    kpackagetool5 -t Plasma/Wallpaper -i jcWallpaper

## Removal

    kpackagetool5 -r jcWallpaper
